package br.com.android.ciclodevida;

import java.security.acl.LastOwnerException;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {
	public static final String CATEGORIA = "LIVRO";

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_main);
        Log.i(CATEGORIA, getClassName() + ".onCreate() chamado : "+ icicle);
        TextView t = new TextView(this);
        t.setText("Ciclo de Vida android \n Consulte LogCat");
        setContentView(t);
    }


    protected void onStart(){
    	super.onStart();
    	Log.i(CATEGORIA, getClassName() +".onStart() chamado");
    }
    
    protected void onPause(){
    	super.onPause();
    	Log.i(CATEGORIA, getClassName() +".onPause() chamado");
    }
    
    protected void onStop(){
    	super.onStop();
    	Log.i(CATEGORIA, getClassName() +".onStop() chamado");
    }
    
    protected void onDestroy(){
    	super.onDestroy();
    	Log.i(CATEGORIA, getClassName() +".onDestroy() chamado");
    }
    
    private String getClassName(){
    	String s = getClass().getName();
    	return s.substring(s.lastIndexOf("."));
    }
    
}
